﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Common;

namespace MultiFilteredDataGridMVVM.Design
{
    public class DesignDummyService : IDataService
    {
        public List<Thing> GetThings()
        {
            List<Thing> results = new List<Thing>();

            var thing = new Thing
            {
                Id = 22,
                Year = 2011,
                Author = "Rover Blue",
                FileName = "Blah.txt",
                LastUpdated = DateTime.Now,
                Country = "Canada",
                Title = "WooHoo #1"
            };
            results.Add(thing);

            thing = new Thing
            {
                Id = 34,
                Year = 2011,
                Author = "Jimmy Bobb",
                FileName = "FOO.txt",
                LastUpdated = DateTime.Now,
                Country = "Canada",
                Title = "WooHoo #2"
            };
            results.Add(thing);

            thing = new Thing
            {
                Id = 34,
                Year = 2011,
                Author = "Jimmy Bobb Blue",
                FileName = "FOO2.txt",
                LastUpdated = DateTime.Now,
                Country = "Canada",
                Title = "WooHoo #3"
            };
            results.Add(thing);

            return results;
        }
    }
}