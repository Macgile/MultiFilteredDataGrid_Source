﻿using System.CodeDom;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Common;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MultiFilteredDataGridMVVM.Helpers;

// ReSharper disable UseNameofExpression

namespace MultiFilteredDataGridMVVM.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Members

        private string selectedAuthor;
        private string selectedCountry;
        private int? selectedYear;
        private ObservableCollection<string> authors;
        private ObservableCollection<string> countries;
        private ObservableCollection<int> years;
        private ObservableCollection<Thing> things;
        private bool canCanRemoveCountryFilter;
        private bool canCanRemoveAuthorFilter;
        private bool canCanRemoveYearFilter;

        #endregion

        public MainViewModel(IDataService dataService)
        {
            InitializeCommands();
            DataService = dataService;
            LoadData();

            //--------------------------------------------------------------
            //This 'registers' the instance of this view model to recieve messages with this type of token.This
            //is used to recieve a reference from the view that the collectionViewSource has been instantiated
            // and to recieve a reference to the CollectionViewSource which will be used in the view model for
            // filtering
            Messenger.Default.Register<ViewCollectionViewSourceMessageToken>(this,
                Handle_ViewCollectionViewSourceMessageToken);
        }

        public override void Cleanup()
        {
            Messenger.Default.Unregister<ViewCollectionViewSourceMessageToken>(this);
            base.Cleanup();
        }

        /// <summary>
        /// Gets or sets the IDownloadDataService member
        /// </summary>
        private IDataService DataService { get; set; }

        /// <summary>
        /// Gets or sets the CollectionViewSource which is the proxy for the 
        /// collection of Things and the datagrid in which each thing is displayed.
        /// </summary>
        private CollectionViewSource Cvs { get; set; }

        #region Properties (Displayable in View)

        /// <summary>
        /// Gets or sets the primary collection of Thing objects to be displayed
        /// </summary>
        public ObservableCollection<Thing> Things
        {
            get { return things; }
            set
            {
                if (things == value)
                    return;
                things = value;
                RaisePropertyChanged("Things");
            }
        }

        // Filter properties =============

        /// <summary>
        /// Gets or sets the selected author in the list authors to filter the collection
        /// </summary>
        public string SelectedAuthor
        {
            get { return selectedAuthor; }
            set
            {
                if (selectedAuthor == value) return;
                selectedAuthor = value;
                RaisePropertyChanged("SelectedAuthor");
                ApplyFilter(!string.IsNullOrEmpty(selectedAuthor) ? FilterField.Author : FilterField.None);
            }
        }

        /// <summary>
        /// Gets or sets the selected author in the list countries to filter the collection
        /// </summary>
        public string SelectedCountry
        {
            get { return selectedCountry; }
            set
            {
                if (selectedCountry == value) return;
                selectedCountry = value;
                RaisePropertyChanged("SelectedCountry");
                ApplyFilter(!string.IsNullOrEmpty(selectedCountry) ? FilterField.Country : FilterField.None);
            }
        }

        /// <summary>
        /// Gets or sets the selected author in the list years to filter the collection
        /// </summary>
        public int? SelectedYear
        {
            get { return selectedYear; }
            set
            {
                if (selectedYear == value) return;
                selectedYear = value;
                RaisePropertyChanged("SelectedYear");
                ApplyFilter(selectedYear.HasValue ? FilterField.Year : FilterField.None);
            }
        }

        /// <summary>
        /// Gets or sets a list of authors which is used to populate the author filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<string> Authors
        {
            get { return authors; }
            set
            {
                if (authors == value) return;
                authors = value;

                RaisePropertyChanged("Authors");
            }
        }

        /// <summary>
        /// Gets or sets a list of authors which is used to populate the country filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<string> Countries
        {
            get { return countries; }
            set
            {
                if (countries == value) return;
                countries = value;
                RaisePropertyChanged("Countries");
            }
        }

        /// <summary>
        /// Gets or sets a list of authors which is used to populate the year filter
        /// drop down list.
        /// </summary>
        public ObservableCollection<int> Years
        {
            get { return years; }
            set
            {
                if (years == value) return;
                years = value;
                RaisePropertyChanged("Years");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Country filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveCountryFilter
        {
            get { return canCanRemoveCountryFilter; }
            set
            {
                canCanRemoveCountryFilter = value;
                RaisePropertyChanged("CanRemoveCountryFilter");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Author filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveAuthorFilter
        {
            get { return canCanRemoveAuthorFilter; }
            set
            {
                canCanRemoveAuthorFilter = value;
                RaisePropertyChanged("CanRemoveAuthorFilter");
            }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the Year filter, if applied, can be removed.
        /// </summary>
        public bool CanRemoveYearFilter
        {
            get { return canCanRemoveYearFilter; }
            set
            {
                canCanRemoveYearFilter = value;
                RaisePropertyChanged("CanRemoveYearFilter");
            }
        }

        #endregion

        #region Commands

        public ICommand ResetFiltersCommand { get; private set; }
        public ICommand RemoveCountryFilterCommand { get; private set; }
        public ICommand RemoveAuthorFilterCommand { get; private set; }
        public ICommand RemoveYearFilterCommand { get; private set; }

        #endregion

        private void InitializeCommands()
        {
            ResetFiltersCommand = new RelayCommand(ResetFilters, null);
            RemoveCountryFilterCommand = new RelayCommand(RemoveCountryFilter, () => CanRemoveCountryFilter);
            RemoveAuthorFilterCommand = new RelayCommand(RemoveAuthorFilter, () => CanRemoveAuthorFilter);
            RemoveYearFilterCommand = new RelayCommand(RemoveYearFilter, () => CanRemoveYearFilter);
        }

        private void LoadData()
        {
            // Main collection of Data
            var thingsData = DataService.GetThings();

            var q1 = from t in thingsData select t.Author;
            Authors = new ObservableCollection<string>(q1.Distinct());

            var q2 = from t in thingsData select t.Country;
            Countries = new ObservableCollection<string>(q2.Distinct());

            var q3 = from t in thingsData select t.Year;
            Years = new ObservableCollection<int>(q3.Distinct());

            Things = new ObservableCollection<Thing>(thingsData);

            //var toto = from t in Things select t.Author;

            //var auteur = new ObservableCollection<string>(toto.Distinct());

            //Debug.WriteLine(auteur.Count);
        }

        /// <summary>
        /// This method handles a message recieved from the View which enables a reference to the
        /// instantiated CollectionViewSource to be used in the ViewModel.
        /// </summary>
        private void Handle_ViewCollectionViewSourceMessageToken(ViewCollectionViewSourceMessageToken token)
        {
            Cvs = token.CVS;
        }

        // Command methods (called by the commands) ===============

        public void ResetFilters()
        {
            // clear filters 
            RemoveYearFilter();
            RemoveAuthorFilter();
            RemoveCountryFilter();
        }

        public void RemoveCountryFilter()
        {
            //var view = Cvs.View.Cast<Thing>().Count();
            Cvs.Filter -= FilterByCountry;
            //view = Cvs.View.Cast<Thing>().Count();

            SelectedCountry = null;
            CanRemoveCountryFilter = false;
        }

        public void RemoveAuthorFilter()
        {
            Cvs.Filter -= FilterByAuthor;
            SelectedAuthor = null;
            CanRemoveAuthorFilter = false;
        }

        public void RemoveYearFilter()
        {
            Cvs.Filter -= FilterByYear;
            SelectedYear = null;
            CanRemoveYearFilter = false;
        }

        // Other helper methods ==============

        /*   Notes on Adding Filters:
         *   Each filter is added by subscribing a filter method to the Filter event
         *   of the CVS.  Filters are applied in the order in which they were added. 
         *   To prevent adding filters mulitple times ( because we are using drop down lists
         *   in the view), the CanRemove***Filter flags are used to ensure each filter
         *   is added only once.  If a filter has been added, its corresponding CanRemove***Filter
         *   is set to true.       
         *   
         *   If a filter has been applied already (for example someone selects "Canada" to filter by country
         *   and then they change their selection to another value (say "Mexico") we need to undo the previous
         *   country filter then apply the new one.  This does not completey Reset the filter, just
         *   allows it to be changed to another filter value. This applies to the other filters as well
         */

        public void AddCountryFilter()
        {
            // see Notes on Adding Filters:
            if (CanRemoveCountryFilter)
            {
                Cvs.Filter -= FilterByCountry;
                Cvs.Filter += FilterByCountry;
            }
            else
            {
                Cvs.Filter += FilterByCountry;

                var items = Cvs.View.Cast<Thing>().Select(t => t.Country).Distinct();


                var q1 = from t in things where items.Contains(t.Country) select t.Author;
                Authors = new ObservableCollection<string>(q1.Distinct());

                var q2 = from t in things where items.Contains(t.Country) select t.Year;
                Years = new ObservableCollection<int>(q2.Distinct());

                CanRemoveCountryFilter = true;
            }
        }

        public void AddAuthorFilter()
        {
            // see Notes on Adding Filters:
            if (CanRemoveAuthorFilter)
            {
                Cvs.Filter -= FilterByAuthor;
                Cvs.Filter += FilterByAuthor;
            }
            else
            {
                var authorFilter = Authors.Count;

                Cvs.Filter += FilterByAuthor;

                authorFilter = Authors.Count;

                var iAuthor = Cvs.View.Cast<Thing>().Select(t => t.Author).Distinct();


                var q1 = (from t in things where iAuthor.Contains(t.Author) select t.Year).Distinct();
                Years = new ObservableCollection<int>(q1);

                var q2 = (from t in things where iAuthor.Contains(t.Author) select t.Country).Distinct();
                Countries = new ObservableCollection<string>(q2);

                CanRemoveAuthorFilter = true;
            }
        }

        public void AddYearFilter()
        {
            // see Notes on Adding Filters:
            if (CanRemoveYearFilter)
            {
                Cvs.Filter -= FilterByYear;
                Cvs.Filter += FilterByYear;
            }
            else
            {
                var count = Cvs.View.Cast<Thing>().Count();

                // iterate over Cvs collection and filter by Years
                Cvs.Filter += FilterByYear;

                var items = Cvs.View.Cast<Thing>().Select(t => t.Year).Distinct();


                var q1 = from t in things where items.Contains(t.Year) select t.Author;
                Authors = new ObservableCollection<string>(q1.Distinct());

                var q2 = from t in things where items.Contains(t.Year) select t.Country;
                Countries = new ObservableCollection<string>(q2.Distinct());

                //Debug.WriteLine(years.Count);

                CanRemoveYearFilter = true;
            }
        }

        /* Notes on Filter Methods:
         * When using multiple filters, do not explicitly set anything to true.  Rather,
         * only hide things which do not match the filter criteria
         * by setting e.Accepted = false.  If you set e.Accept = true, if effectively
         * clears out any previous filters applied to it.  
         */

        private void FilterByAuthor(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (string.CompareOrdinal(SelectedAuthor, src.Author) != 0)
                e.Accepted = false;
        }

        private void FilterByYear(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (SelectedYear != src.Year)
                e.Accepted = false;
        }

        private void FilterByCountry(object sender, FilterEventArgs e)
        {
            // see Notes on Filter Methods:
            var src = e.Item as Thing;
            if (src == null)
                e.Accepted = false;
            else if (string.CompareOrdinal(SelectedCountry, src.Country) != 0)
                e.Accepted = false;
        }

        private enum FilterField
        {
            Author,
            Country,
            Year,
            None
        }

        private void ApplyFilter(FilterField field)
        {
            switch (field)
            {
                case FilterField.Author:
                    AddAuthorFilter();
                    break;
                case FilterField.Country:
                    AddCountryFilter();
                    break;
                case FilterField.Year:
                    AddYearFilter();
                    break;
            }
        }
    }
}